package com.exploritage.util;

import android.view.MotionEvent;

/**
 * Created by ajay on 1/3/17.
 */

public interface ScaleListener {
    public boolean onTouchEvent(MotionEvent ev);

}
